cmake_minimum_required(VERSION 3.12)
project(Pr1_Bl2_AG4 C)

set(CMAKE_C_STANDARD 99)

add_executable(Pr1_Bl2_AG4 main.c)