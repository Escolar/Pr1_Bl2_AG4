#include <stdio.h>

int fak1 (int);
int fak2 (int);

int main() {
    int zahl;

    printf("Bitte geben Sie die zu fakultierende Zahl ein: ");
    scanf("%i", &zahl);

    if(zahl < 0) {
        printf("Die Zahl muss positiv sein!");
        return 0;
    }

    printf("Fakultaet von %i: %i \n", zahl, fak1(zahl));
    printf("Fakultaet von %i: %i \n", zahl, fak2(zahl));
    return 0;
}

int fak1 (int x) {
    if(x == 0) return 1;

    return x * fak1(x - 1);
}

int fak2(int x) {
    return x != 0 ? x * fak2(x - 1) : 1;
}